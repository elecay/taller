Gestor de clientes y trabajos para un taller de chapa y pintura
===============================================================

Instalación necesaria
---------------------

Este proyecto utiliza [Spring Roo v1.2.1](http://www.springsource.org/spring-roo) y [PrimeFaces 3.1](http://www.primefaces.org/). Se puede trabajar simplemente desde la consola, pero es recomendable utilizar el entorno de desarrollo ofrecido por _VMware_ [SpringSource Tool Suite](http://www.springsource.org/sts).

El proyecto actualmente se está probando en [Cloudbees](http://www.cloudbees.com) el que para aplicaciones JEE utiliza un servidor JBoss, por lo que se recomienda ejecutarlo localmente en [JBoss AS 7](http://www.jboss.org/jbossas/downloads/). También puede utilizar Tomcat 7. Es importante tener en cuenta que se está utilizando JSF 2.0 con EL 2.2, lo que es soportado a partir de la versión 7 de Tomcat.

Como DB se utiliza MySql pero puede configurarse fácilmente cualquier otra desde `persistence.xml`. 

Importante
----------

* No modificar los archivos `AspectJ` (.aj) ya que son manejados automáticamente por `Roo`.
* Las anotaciones @ManagedBean y @FacesConverter deben colocarse manualmente en los archivos `.java` para que `Roo` las remueva de los .aj. Se debe realizar esto ya que JBoss lanza excepciones.

Autor
-----

* @elecay [Sebastián Rajo](mailto:elecay@gmail.com)

Comments
--------

Testing Jenkins with Bitbucket (8th try)

Copyright
---------

(c) 2012 [Sebastián Rajo](http://www.prototipo.com.uy), Prototipo
