// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.sebastian;

import com.sebastian.ManoObraDataOnDemand;
import com.sebastian.model.ManoObra;
import com.sebastian.model.TipoTrabajo;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.springframework.stereotype.Component;

privileged aspect ManoObraDataOnDemand_Roo_DataOnDemand {
    
    declare @type: ManoObraDataOnDemand: @Component;
    
    private Random ManoObraDataOnDemand.rnd = new SecureRandom();
    
    private List<ManoObra> ManoObraDataOnDemand.data;
    
    public ManoObra ManoObraDataOnDemand.getNewTransientManoObra(int index) {
        ManoObra obj = new ManoObra();
        setDescripcion(obj, index);
        setTipoTrabajo(obj, index);
        return obj;
    }
    
    public void ManoObraDataOnDemand.setDescripcion(ManoObra obj, int index) {
        String descripcion = "descripcion_" + index;
        obj.setDescripcion(descripcion);
    }
    
    public void ManoObraDataOnDemand.setTipoTrabajo(ManoObra obj, int index) {
        TipoTrabajo tipoTrabajo = null;
        obj.setTipoTrabajo(tipoTrabajo);
    }
    
    public ManoObra ManoObraDataOnDemand.getSpecificManoObra(int index) {
        init();
        if (index < 0) {
            index = 0;
        }
        if (index > (data.size() - 1)) {
            index = data.size() - 1;
        }
        ManoObra obj = data.get(index);
        Long id = obj.getId();
        return ManoObra.findManoObra(id);
    }
    
    public ManoObra ManoObraDataOnDemand.getRandomManoObra() {
        init();
        ManoObra obj = data.get(rnd.nextInt(data.size()));
        Long id = obj.getId();
        return ManoObra.findManoObra(id);
    }
    
    public boolean ManoObraDataOnDemand.modifyManoObra(ManoObra obj) {
        return false;
    }
    
    public void ManoObraDataOnDemand.init() {
        int from = 0;
        int to = 10;
        data = ManoObra.findManoObraEntries(from, to);
        if (data == null) {
            throw new IllegalStateException("Find entries implementation for 'ManoObra' illegally returned null");
        }
        if (!data.isEmpty()) {
            return;
        }
        
        data = new ArrayList<ManoObra>();
        for (int i = 0; i < 10; i++) {
            ManoObra obj = getNewTransientManoObra(i);
            try {
                obj.persist();
            } catch (ConstraintViolationException e) {
                StringBuilder msg = new StringBuilder();
                for (Iterator<ConstraintViolation<?>> iter = e.getConstraintViolations().iterator(); iter.hasNext();) {
                    ConstraintViolation<?> cv = iter.next();
                    msg.append("[").append(cv.getConstraintDescriptor()).append(":").append(cv.getMessage()).append("=").append(cv.getInvalidValue()).append("]");
                }
                throw new RuntimeException(msg.toString(), e);
            }
            obj.flush();
            data.add(obj);
        }
    }
    
}
