package com.sebastian.service;

import java.io.InputStream;

public interface MediaService {

	public boolean connect(String uri, String user, String password);

	public InputStream pull(String file_id, String file_type);

	public String push(InputStream inputStream, String file_id);

}
