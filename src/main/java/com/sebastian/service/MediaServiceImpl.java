package com.sebastian.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.springframework.roo.addon.javabean.RooJavaBean;

@RooJavaBean
public class MediaServiceImpl implements MediaService {

	private static MediaServiceImpl instance = null;
	private String PATH = System.getProperty("java.io.tmpdir");

	protected MediaServiceImpl() {

	}

	public static MediaServiceImpl getInstance() {
		if (instance == null) {
			instance = new MediaServiceImpl();
		}
		return instance;
	}

	@Override
	public boolean connect(String uri, String user, String password) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public InputStream pull(String file_id, String file_type) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String push(InputStream inputStream, String file_id) {

		String path = PATH + "/" + file_id;
		File f = new File(path);
		OutputStream oS;
		try {
			oS = new FileOutputStream(f);
			byte[] buf = new byte[1024];
			int len;
			while ((len = inputStream.read(buf)) > -1) {
				oS.write(buf, 0, len);
			}
			oS.close();
			inputStream.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return file_id;
	}

}
