package com.sebastian.web;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.faces.application.Application;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.primefaces.component.menuitem.MenuItem;
import org.primefaces.component.submenu.Submenu;
import org.primefaces.model.DefaultMenuModel;
import org.primefaces.model.MenuModel;
import org.springframework.roo.addon.jsf.application.RooJsfApplicationBean;

@ManagedBean
@RooJsfApplicationBean
public class ApplicationBean {

	private MenuModel menuModel;

	@PostConstruct
	public void init() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		Application application = facesContext.getApplication();
		ExpressionFactory expressionFactory = application
				.getExpressionFactory();
		ELContext elContext = facesContext.getELContext();

		menuModel = new DefaultMenuModel();
		Submenu submenu;
		MenuItem item;

		submenu = new Submenu();
		submenu.setId("aseguradoraSubmenu");
		submenu.setLabel("Aseguradora");
		item = new MenuItem();
		item.setId("createAseguradoraMenuItem");
		item.setValueExpression("value", expressionFactory
				.createValueExpression(elContext, "#{messages.label_create}",
						String.class));
		item.setActionExpression(expressionFactory.createMethodExpression(
				elContext, "#{aseguradoraBean.displayCreateDialog}",
				String.class, new Class[0]));
		item.setIcon("ui-icon ui-icon-document");
		item.setAjax(false);
		item.setAsync(false);
		item.setUpdate(":dataForm:data");
		submenu.getChildren().add(item);
		item = new MenuItem();
		item.setId("listAseguradoraMenuItem");
		item.setValueExpression("value", expressionFactory
				.createValueExpression(elContext, "#{messages.label_list}",
						String.class));
		item.setActionExpression(expressionFactory.createMethodExpression(
				elContext, "#{aseguradoraBean.displayList}", String.class,
				new Class[0]));
		item.setIcon("ui-icon ui-icon-folder-open");
		item.setAjax(false);
		item.setAsync(false);
		item.setUpdate(":dataForm:data");
		submenu.getChildren().add(item);
		menuModel.addSubmenu(submenu);

		submenu = new Submenu();
		submenu.setId("autoSubmenu");
		submenu.setLabel("Auto");
		item = new MenuItem();
		item.setId("createAutoMenuItem");
		item.setValueExpression("value", expressionFactory
				.createValueExpression(elContext, "#{messages.label_create}",
						String.class));
		item.setActionExpression(expressionFactory.createMethodExpression(
				elContext, "#{autoBean.displayCreateDialog}", String.class,
				new Class[0]));
		item.setIcon("ui-icon ui-icon-document");
		item.setAjax(false);
		item.setAsync(false);
		item.setUpdate(":dataForm:data");
		submenu.getChildren().add(item);
		item = new MenuItem();
		item.setId("listAutoMenuItem");
		item.setValueExpression("value", expressionFactory
				.createValueExpression(elContext, "#{messages.label_list}",
						String.class));
		item.setActionExpression(expressionFactory.createMethodExpression(
				elContext, "#{autoBean.displayList}", String.class,
				new Class[0]));
		item.setIcon("ui-icon ui-icon-folder-open");
		item.setAjax(false);
		item.setAsync(false);
		item.setUpdate(":dataForm:data");
		submenu.getChildren().add(item);
		menuModel.addSubmenu(submenu);

		submenu = new Submenu();
		submenu.setId("clienteSubmenu");
		submenu.setLabel("Cliente");
		item = new MenuItem();
		item.setId("createClienteMenuItem");
		item.setValueExpression("value", expressionFactory
				.createValueExpression(elContext, "#{messages.label_create}",
						String.class));
		item.setActionExpression(expressionFactory.createMethodExpression(
				elContext, "#{clienteBean.displayCreateDialog}", String.class,
				new Class[0]));
		item.setIcon("ui-icon ui-icon-document");
		item.setAjax(false);
		item.setAsync(false);
		item.setUpdate(":dataForm:data");
		submenu.getChildren().add(item);
		item = new MenuItem();
		item.setId("listClienteMenuItem");
		item.setValueExpression("value", expressionFactory
				.createValueExpression(elContext, "#{messages.label_list}",
						String.class));
		item.setActionExpression(expressionFactory.createMethodExpression(
				elContext, "#{clienteBean.displayList}", String.class,
				new Class[0]));
		item.setIcon("ui-icon ui-icon-folder-open");
		item.setAjax(false);
		item.setAsync(false);
		item.setUpdate(":dataForm:data");
		submenu.getChildren().add(item);
		menuModel.addSubmenu(submenu);

		submenu = new Submenu();
		submenu.setId("comercioSubmenu");
		submenu.setLabel("Comercio");
		item = new MenuItem();
		item.setId("createComercioMenuItem");
		item.setValueExpression("value", expressionFactory
				.createValueExpression(elContext, "#{messages.label_create}",
						String.class));
		item.setActionExpression(expressionFactory.createMethodExpression(
				elContext, "#{comercioBean.displayCreateDialog}", String.class,
				new Class[0]));
		item.setIcon("ui-icon ui-icon-document");
		item.setAjax(false);
		item.setAsync(false);
		item.setUpdate(":dataForm:data");
		submenu.getChildren().add(item);
		item = new MenuItem();
		item.setId("listComercioMenuItem");
		item.setValueExpression("value", expressionFactory
				.createValueExpression(elContext, "#{messages.label_list}",
						String.class));
		item.setActionExpression(expressionFactory.createMethodExpression(
				elContext, "#{comercioBean.displayList}", String.class,
				new Class[0]));
		item.setIcon("ui-icon ui-icon-folder-open");
		item.setAjax(false);
		item.setAsync(false);
		item.setUpdate(":dataForm:data");
		submenu.getChildren().add(item);
		menuModel.addSubmenu(submenu);

		/*
		submenu = new Submenu();
		submenu.setId("lineaCompraSubmenu");
		submenu.setLabel("LineaCompra");
		item = new MenuItem();
		item.setId("createLineaCompraMenuItem");
		item.setValueExpression("value", expressionFactory
				.createValueExpression(elContext, "#{messages.label_create}",
						String.class));
		item.setActionExpression(expressionFactory.createMethodExpression(
				elContext, "#{lineaCompraBean.displayCreateDialog}",
				String.class, new Class[0]));
		item.setIcon("ui-icon ui-icon-document");
		item.setAjax(false);
		item.setAsync(false);
		item.setUpdate(":dataForm:data");
		submenu.getChildren().add(item);
		item = new MenuItem();
		item.setId("listLineaCompraMenuItem");
		item.setValueExpression("value", expressionFactory
				.createValueExpression(elContext, "#{messages.label_list}",
						String.class));
		item.setActionExpression(expressionFactory.createMethodExpression(
				elContext, "#{lineaCompraBean.displayList}", String.class,
				new Class[0]));
		item.setIcon("ui-icon ui-icon-folder-open");
		item.setAjax(false);
		item.setAsync(false);
		item.setUpdate(":dataForm:data");
		submenu.getChildren().add(item);
		menuModel.addSubmenu(submenu);

		submenu = new Submenu();
		submenu.setId("lineaManoObraSubmenu");
		submenu.setLabel("LineaManoObra");
		item = new MenuItem();
		item.setId("createLineaManoObraMenuItem");
		item.setValueExpression("value", expressionFactory
				.createValueExpression(elContext, "#{messages.label_create}",
						String.class));
		item.setActionExpression(expressionFactory.createMethodExpression(
				elContext, "#{lineaManoObraBean.displayCreateDialog}",
				String.class, new Class[0]));
		item.setIcon("ui-icon ui-icon-document");
		item.setAjax(false);
		item.setAsync(false);
		item.setUpdate(":dataForm:data");
		submenu.getChildren().add(item);
		item = new MenuItem();
		item.setId("listLineaManoObraMenuItem");
		item.setValueExpression("value", expressionFactory
				.createValueExpression(elContext, "#{messages.label_list}",
						String.class));
		item.setActionExpression(expressionFactory.createMethodExpression(
				elContext, "#{lineaManoObraBean.displayList}", String.class,
				new Class[0]));
		item.setIcon("ui-icon ui-icon-folder-open");
		item.setAjax(false);
		item.setAsync(false);
		item.setUpdate(":dataForm:data");
		submenu.getChildren().add(item);
		menuModel.addSubmenu(submenu);

		submenu = new Submenu();
		submenu.setId("manoObraSubmenu");
		submenu.setLabel("ManoObra");
		item = new MenuItem();
		item.setId("createManoObraMenuItem");
		item.setValueExpression("value", expressionFactory
				.createValueExpression(elContext, "#{messages.label_create}",
						String.class));
		item.setActionExpression(expressionFactory.createMethodExpression(
				elContext, "#{manoObraBean.displayCreateDialog}", String.class,
				new Class[0]));
		item.setIcon("ui-icon ui-icon-document");
		item.setAjax(false);
		item.setAsync(false);
		item.setUpdate(":dataForm:data");
		submenu.getChildren().add(item);
		item = new MenuItem();
		item.setId("listManoObraMenuItem");
		item.setValueExpression("value", expressionFactory
				.createValueExpression(elContext, "#{messages.label_list}",
						String.class));
		item.setActionExpression(expressionFactory.createMethodExpression(
				elContext, "#{manoObraBean.displayList}", String.class,
				new Class[0]));
		item.setIcon("ui-icon ui-icon-folder-open");
		item.setAjax(false);
		item.setAsync(false);
		item.setUpdate(":dataForm:data");
		submenu.getChildren().add(item);
		menuModel.addSubmenu(submenu);
		*/
		
		submenu = new Submenu();
		submenu.setId("presupuestoSubmenu");
		submenu.setLabel("Presupuesto");
		item = new MenuItem();
		item.setId("createPresupuestoMenuItem");
		item.setValueExpression("value", expressionFactory
				.createValueExpression(elContext, "#{messages.label_create}",
						String.class));
		item.setActionExpression(expressionFactory.createMethodExpression(
				elContext, "#{presupuestoBean.displayCreateDialog}",
				String.class, new Class[0]));
		item.setIcon("ui-icon ui-icon-document");
		item.setAjax(false);
		item.setAsync(false);
		item.setUpdate(":dataForm:data");
		submenu.getChildren().add(item);
		item = new MenuItem();
		item.setId("listPresupuestoMenuItem");
		item.setValueExpression("value", expressionFactory
				.createValueExpression(elContext, "#{messages.label_list}",
						String.class));
		item.setActionExpression(expressionFactory.createMethodExpression(
				elContext, "#{presupuestoBean.displayList}", String.class,
				new Class[0]));
		item.setIcon("ui-icon ui-icon-folder-open");
		item.setAjax(false);
		item.setAsync(false);
		item.setUpdate(":dataForm:data");
		submenu.getChildren().add(item);
		menuModel.addSubmenu(submenu);
		
		submenu = new Submenu();
		submenu.setId("tipoTrabajoSubmenu");
		submenu.setLabel("Tipo de trabajo");
		item = new MenuItem();
		item.setId("createTipoTrabajoMenuItem");
		item.setValueExpression("value", expressionFactory
				.createValueExpression(elContext, "#{messages.label_create}",
						String.class));
		item.setActionExpression(expressionFactory.createMethodExpression(
				elContext, "#{tipoTrabajoBean.displayCreateDialog}",
				String.class, new Class[0]));
		item.setIcon("ui-icon ui-icon-document");
		item.setAjax(false);
		item.setAsync(false);
		item.setUpdate(":dataForm:data");
		submenu.getChildren().add(item);
		item = new MenuItem();
		item.setId("listTipoTrabajoMenuItem");
		item.setValueExpression("value", expressionFactory
				.createValueExpression(elContext, "#{messages.label_list}",
						String.class));
		item.setActionExpression(expressionFactory.createMethodExpression(
				elContext, "#{tipoTrabajoBean.displayList}", String.class,
				new Class[0]));
		item.setIcon("ui-icon ui-icon-folder-open");
		item.setAjax(false);
		item.setAsync(false);
		item.setUpdate(":dataForm:data");
		submenu.getChildren().add(item);
		menuModel.addSubmenu(submenu);
		
		/*
		submenu = new Submenu();
		submenu.setId("repuestoSubmenu");
		submenu.setLabel("Repuesto");
		item = new MenuItem();
		item.setId("createRepuestoMenuItem");
		item.setValueExpression("value", expressionFactory
				.createValueExpression(elContext, "#{messages.label_create}",
						String.class));
		item.setActionExpression(expressionFactory.createMethodExpression(
				elContext, "#{repuestoBean.displayCreateDialog}", String.class,
				new Class[0]));
		item.setIcon("ui-icon ui-icon-document");
		item.setAjax(false);
		item.setAsync(false);
		item.setUpdate(":dataForm:data");
		submenu.getChildren().add(item);
		item = new MenuItem();
		item.setId("listRepuestoMenuItem");
		item.setValueExpression("value", expressionFactory
				.createValueExpression(elContext, "#{messages.label_list}",
						String.class));
		item.setActionExpression(expressionFactory.createMethodExpression(
				elContext, "#{repuestoBean.displayList}", String.class,
				new Class[0]));
		item.setIcon("ui-icon ui-icon-folder-open");
		item.setAjax(false);
		item.setAsync(false);
		item.setUpdate(":dataForm:data");
		submenu.getChildren().add(item);
		menuModel.addSubmenu(submenu);
		*/
	}

	public MenuModel getMenuModel() {
		return menuModel;
	}

	public String getColumnName(String column) {
		if (column == null || column.length() == 0) {
			return column;
		}
		final Pattern p = Pattern.compile("[A-Z][^A-Z]*");
		final Matcher m = p.matcher(Character.toUpperCase(column.charAt(0))
				+ column.substring(1));
		final StringBuilder builder = new StringBuilder();
		while (m.find()) {
			builder.append(m.group()).append(" ");
		}
		return builder.toString().trim();
	}
}