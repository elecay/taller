package com.sebastian.web;

import javax.faces.bean.ManagedBean;

import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

import com.sebastian.model.Aseguradora;

@ManagedBean(name = "aseguradoraBean")
@RooSerializable
@RooJsfManagedBean(entity = Aseguradora.class, beanName = "aseguradoraBean")
public class AseguradoraBean {
}
