package com.sebastian.web;

import javax.faces.bean.ManagedBean;

import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

import com.sebastian.model.LineaCompra;

@ManagedBean(name = "lineaCompraBean")
@RooSerializable
@RooJsfManagedBean(entity = LineaCompra.class, beanName = "lineaCompraBean")
public class LineaCompraBean {
	
	private LineaCompra lineaCompra;
	
	public LineaCompraBean(){
		lineaCompra = new LineaCompra();
	}
	
	public LineaCompra getLineaCompra() {
		return lineaCompra;
	}

	public void setLineaCompra(LineaCompra lineaCompra) {
		this.lineaCompra = lineaCompra;
	}
	
}
