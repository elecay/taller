// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.sebastian.web.converter;

import com.sebastian.model.Comercio;
import com.sebastian.web.converter.ComercioConverter;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

privileged aspect ComercioConverter_Roo_Converter {
    
    declare parents: ComercioConverter implements Converter;
    
    public Object ComercioConverter.getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.length() == 0) {
            return null;
        }
        Long id = Long.parseLong(value);
        return Comercio.findComercio(id);
    }
    
    public String ComercioConverter.getAsString(FacesContext context, UIComponent component, Object value) {
        return value instanceof Comercio ? ((Comercio) value).getId().toString() : "";
    }
    
}
