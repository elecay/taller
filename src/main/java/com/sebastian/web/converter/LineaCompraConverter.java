package com.sebastian.web.converter;

import javax.faces.convert.FacesConverter;

import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

import com.sebastian.model.LineaCompra;

@FacesConverter("com.sebastian.web.converter.LineaCompraConverter")
@RooJsfConverter(entity = LineaCompra.class)
public class LineaCompraConverter {
}
