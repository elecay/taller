// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.sebastian.web.converter;

import com.sebastian.web.converter.ComercioConverter;
import org.springframework.beans.factory.annotation.Configurable;

privileged aspect ComercioConverter_Roo_Configurable {
    
    declare @type: ComercioConverter: @Configurable;
    
}
