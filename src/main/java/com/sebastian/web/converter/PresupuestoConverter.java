package com.sebastian.web.converter;

import javax.faces.convert.FacesConverter;

import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

import com.sebastian.model.Presupuesto;

@FacesConverter("com.sebastian.web.converter.PresupuestoConverter")
@RooJsfConverter(entity = Presupuesto.class)
public class PresupuestoConverter {
}
