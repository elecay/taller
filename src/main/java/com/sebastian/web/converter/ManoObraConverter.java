package com.sebastian.web.converter;

import javax.faces.convert.FacesConverter;

import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

import com.sebastian.model.ManoObra;

@FacesConverter("com.sebastian.web.converter.ManoObraConverter")
@RooJsfConverter(entity = ManoObra.class)
public class ManoObraConverter {
}
