package com.sebastian.web.converter;

import javax.faces.convert.FacesConverter;

import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

import com.sebastian.model.Aseguradora;

@FacesConverter("com.sebastian.web.converter.AseguradoraConverter")
@RooJsfConverter(entity = Aseguradora.class)
public class AseguradoraConverter {
}
