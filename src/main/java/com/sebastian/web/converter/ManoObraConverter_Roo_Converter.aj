// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.sebastian.web.converter;

import com.sebastian.model.ManoObra;
import com.sebastian.web.converter.ManoObraConverter;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

privileged aspect ManoObraConverter_Roo_Converter {
    
    declare parents: ManoObraConverter implements Converter;
    
    public Object ManoObraConverter.getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || value.length() == 0) {
            return null;
        }
        Long id = Long.parseLong(value);
        return ManoObra.findManoObra(id);
    }
    
    public String ManoObraConverter.getAsString(FacesContext context, UIComponent component, Object value) {
        return value instanceof ManoObra ? ((ManoObra) value).getId().toString() : "";
    }
    
}
