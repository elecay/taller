package com.sebastian.web.converter;

import javax.faces.convert.FacesConverter;

import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

import com.sebastian.model.LineaManoObra;

@FacesConverter("com.sebastian.web.converter.LineaManoObraConverter")
@RooJsfConverter(entity = LineaManoObra.class)
public class LineaManoObraConverter {
}
