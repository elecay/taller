package com.sebastian.web.converter;

import javax.faces.convert.FacesConverter;

import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

import com.sebastian.model.Comercio;

@FacesConverter("com.sebastian.web.converter.ComercioConverter")
@RooJsfConverter(entity = Comercio.class)
public class ComercioConverter {
}
