package com.sebastian.web.converter;

import javax.faces.convert.FacesConverter;

import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

import com.sebastian.model.Auto;

@FacesConverter("com.sebastian.web.converter.AutoConverter")
@RooJsfConverter(entity = Auto.class)
public class AutoConverter {
}
