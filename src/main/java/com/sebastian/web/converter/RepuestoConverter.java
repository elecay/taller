package com.sebastian.web.converter;

import javax.faces.convert.FacesConverter;

import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

import com.sebastian.model.Repuesto;

@FacesConverter("com.sebastian.web.converter.RepuestoConverter")
@RooJsfConverter(entity = Repuesto.class)
public class RepuestoConverter {
}
