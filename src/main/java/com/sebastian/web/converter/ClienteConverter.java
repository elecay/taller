package com.sebastian.web.converter;

import javax.faces.convert.FacesConverter;

import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

import com.sebastian.model.Cliente;

@FacesConverter("com.sebastian.web.converter.ClienteConverter")
@RooJsfConverter(entity = Cliente.class)
public class ClienteConverter {
 
}
