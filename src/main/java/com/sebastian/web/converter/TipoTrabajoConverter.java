package com.sebastian.web.converter;

import javax.faces.convert.FacesConverter;

import org.springframework.roo.addon.jsf.converter.RooJsfConverter;

import com.sebastian.model.TipoTrabajo;

@FacesConverter("com.sebastian.web.converter.TipoTrabajoConverter")
@RooJsfConverter(entity = TipoTrabajo.class)
public class TipoTrabajoConverter {
}
