package com.sebastian.web;

import javax.faces.bean.ManagedBean;

import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

import com.sebastian.model.ManoObra;

@ManagedBean(name = "manoObraBean")
@RooSerializable
@RooJsfManagedBean(entity = ManoObra.class, beanName = "manoObraBean")
public class ManoObraBean {
	
}
