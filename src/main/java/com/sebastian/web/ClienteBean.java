package com.sebastian.web;

import javax.faces.bean.ManagedBean;

import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

import com.sebastian.model.Cliente;

@ManagedBean(name = "clienteBean")
@RooSerializable
@RooJsfManagedBean(entity = Cliente.class, beanName = "clienteBean")
public class ClienteBean {
}
