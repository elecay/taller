package com.sebastian.web;

import javax.faces.bean.ManagedBean;

import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

import com.sebastian.model.Repuesto;

@ManagedBean(name = "repuestoBean")
@RooSerializable
@RooJsfManagedBean(entity = Repuesto.class, beanName = "repuestoBean")
public class RepuestoBean {
}
