package com.sebastian.web;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;

import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

import com.sebastian.model.LineaCompra;
import com.sebastian.model.LineaManoObra;
import com.sebastian.model.Presupuesto;

@ManagedBean(name = "presupuestoBean")
@RooSerializable
@RooJsfManagedBean(entity = Presupuesto.class, beanName = "presupuestoBean")
public class PresupuestoBean {

	private List<LineaManoObra> selectedListaManoObra;
	private List<LineaCompra> selectedListaCompras;
	private Presupuesto presupuesto;

	/* Beans attributes */

	@ManagedProperty("#{lineaManoObraBean}")
	private LineaManoObraBean lineaManoObraBean;
	@ManagedProperty("#{lineaCompraBean}")
	private LineaCompraBean lineaCompraBean;
	@ManagedProperty("#{mediaBean}")
	private MediaBean mediaBean;

	public PresupuestoBean() {
		presupuesto = new Presupuesto();
	}

	public void addLineaManoObra() {
		LineaManoObra newLine = new LineaManoObra();
		newLine = lineaManoObraVOtoLine();
		presupuesto.setTotalManoObra(presupuesto.getTotalManoObra()
				+ newLine.getPrecioTotal());
		updatePrecioTotal();
		presupuesto.getListaManoObra().add(newLine);
	}

	public void addLineaCompra() {
		LineaCompra newLine = new LineaCompra();
		newLine = lineaCompraVOtoLine();
		presupuesto.setTotalCompra(presupuesto.getTotalCompra()
				+ newLine.getPrecioTotal());
		updatePrecioTotal();
		presupuesto.getListaCompras().add(newLine);
	}

	private void updatePrecioTotal() {
		presupuesto.setPrecioTotal(presupuesto.getTotalCompra()
				+ presupuesto.getTotalManoObra());
	}

	private LineaCompra lineaCompraVOtoLine() {
		LineaCompra newLine = new LineaCompra();
		newLine.setCantidad(lineaCompraBean.getLineaCompra().getCantidad());
		newLine.setCodigo(lineaCompraBean.getLineaCompra().getCodigo());
		newLine.setComercio(lineaCompraBean.getLineaCompra().getComercio());
		newLine.setDescripcion(lineaCompraBean.getLineaCompra()
				.getDescripcion());
		newLine.setPrecio(lineaCompraBean.getLineaCompra().getPrecio());
		newLine.setPrecioTotal(calcularPrecioLineaCompra());
		lineaCompraBean.setLineaCompra(new LineaCompra());
		return newLine;
	}

	private LineaManoObra lineaManoObraVOtoLine() {
		LineaManoObra newLine = new LineaManoObra();
		newLine.setHoras(lineaManoObraBean.getLineaManoObra().getHoras());
		newLine.setManoDeObra(lineaManoObraBean.getLineaManoObra()
				.getManoDeObra());
		newLine.setPrecioTotal(calcularPrecioLineaManoObra());
		lineaManoObraBean.setLineaManoObra(new LineaManoObra());
		return newLine;
	}

	private Double calcularPrecioLineaManoObra() {
		return (lineaManoObraBean.getLineaManoObra().getManoDeObra()
				.getTipoTrabajo().getPrecioHora() * lineaManoObraBean
				.getLineaManoObra().getHoras());
	}

	private Double calcularPrecioLineaCompra() {
		return (lineaCompraBean.getLineaCompra().getPrecio() * lineaCompraBean
				.getLineaCompra().getCantidad());
	}

	public void handleFileUpload(FileUploadEvent event) {
		mediaBean.upload(event.getFile());
		presupuesto.getImagesPath().add(event.getFile().getFileName());
		mediaBean = new MediaBean();
	}

	public String persist() {
		String message = "";
		if (presupuesto.getId() != null) {
			presupuesto.merge();
			message = "Successfully updated";
		} else {
			presupuesto.persist();
			message = "Successfully created";
		}
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("createDialog.hide()");
		context.execute("editDialog.hide()");

		FacesMessage facesMessage = new FacesMessage(message);
		FacesContext.getCurrentInstance().addMessage(null, facesMessage);
		reset();
		presupuesto = new Presupuesto();
		return findAllPresupuestoes();
	}

	public void setSelectedListaManoObra(
			List<LineaManoObra> selectedListaManoObra) {
		if (selectedListaManoObra != null) {
			presupuesto.setListaManoObra(new ArrayList<LineaManoObra>(
					selectedListaManoObra));
		}
		this.selectedListaManoObra = selectedListaManoObra;
	}

	public LineaManoObraBean getLineaManoObraBean() {
		return lineaManoObraBean;
	}

	public void setLineaManoObraBean(LineaManoObraBean lineaManoObraBean) {
		this.lineaManoObraBean = lineaManoObraBean;
	}

	public LineaCompraBean getLineaCompraBean() {
		return lineaCompraBean;
	}

	public void setLineaCompraBean(LineaCompraBean lineaCompraBean) {
		this.lineaCompraBean = lineaCompraBean;
	}

	public void setSelectedListaCompras(List<LineaCompra> selectedListaCompras) {
		if (selectedListaCompras != null) {
			presupuesto.setListaCompras(new ArrayList<LineaCompra>(
					selectedListaCompras));
		}
		this.selectedListaCompras = selectedListaCompras;
	}

	public MediaBean getMediaBean() {
		return mediaBean;
	}

	public void setMediaBean(MediaBean mediaBean) {
		this.mediaBean = mediaBean;
	}

	public Presupuesto getPresupuesto() {
		return presupuesto;
	}

	public void setPresupuesto(Presupuesto presupuesto) {
		this.presupuesto = presupuesto;
	}

	public List<String> getAllImages() {
		if (presupuesto != null) {
			return presupuesto.getImagesPath();
		}
		return null;
	}
}
