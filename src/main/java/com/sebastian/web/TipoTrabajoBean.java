package com.sebastian.web;

import java.util.List;

import javax.faces.bean.ManagedBean;

import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

import com.sebastian.model.TipoTrabajo;

@ManagedBean(name = "tipoTrabajoBean")
@RooSerializable
@RooJsfManagedBean(entity = TipoTrabajo.class, beanName = "tipoTrabajoBean")
public class TipoTrabajoBean {

	private List<TipoTrabajo> allTipoTrabajoes;

	public List<TipoTrabajo> getAllTipoTrabajoes() {
		return allTipoTrabajoes = TipoTrabajo.findAllTipoTrabajoes();
	}

	public void setAllTipoTrabajoes(List<TipoTrabajo> allTipoTrabajoes) {
		this.allTipoTrabajoes = allTipoTrabajoes;
	}

}
