// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.sebastian.web;

import com.sebastian.web.LineaManoObraBean;
import java.io.Serializable;

privileged aspect LineaManoObraBean_Roo_Serializable {
    
    declare parents: LineaManoObraBean implements Serializable;
    
    private static final long LineaManoObraBean.serialVersionUID = 1L;
    
}
