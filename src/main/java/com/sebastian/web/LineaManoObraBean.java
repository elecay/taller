package com.sebastian.web;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;

import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

import com.sebastian.model.LineaManoObra;

@ManagedBean(name = "lineaManoObraBean")
@RooSerializable
@RooJsfManagedBean(entity = LineaManoObra.class, beanName = "lineaManoObraBean")
public class LineaManoObraBean {

	private LineaManoObra lineaManoObra;
	
	@ManagedProperty("#{manoObraBean}")
	private ManoObraBean manoObraBean;

	public LineaManoObraBean(){
		lineaManoObra = new LineaManoObra();
	}

	public LineaManoObra getLineaManoObra() {
		return lineaManoObra;
	}
	
	public ManoObraBean getManoObraBean() {
		return manoObraBean;
	}

	public void setManoObraBean(ManoObraBean manoObraBean) {
		this.manoObraBean = manoObraBean;
	}

	public void setLineaManoObra(LineaManoObra lineaManoObra) {
		this.lineaManoObra = lineaManoObra;
	}
}
