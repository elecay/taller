package com.sebastian.web;

import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;

import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

import com.sebastian.model.Aseguradora;
import com.sebastian.model.Auto;
import com.sebastian.model.Cliente;

@ManagedBean(name = "autoBean")
@RooSerializable
@RooJsfManagedBean(entity = Auto.class, beanName = "autoBean")
public class AutoBean {

	public List<Cliente> completeCliente(String query) {
		List<Cliente> suggestions = new ArrayList<Cliente>();
		for (Cliente cliente : Cliente.findAllClientes()) {
			String clienteStrNombre = String.valueOf(cliente.getNombre());
			String clienteStrApellido = String.valueOf(cliente.getApellido());
			String clienteStrCedula = String.valueOf(cliente.getCedula());
			if (clienteStrNombre.toLowerCase().startsWith(query.toLowerCase())
					|| clienteStrApellido.toLowerCase().startsWith(
							query.toLowerCase())
					|| clienteStrCedula.toLowerCase().startsWith(
							query.toLowerCase())) {
				suggestions.add(cliente);
			}
		}
		return suggestions;
	}

	public List<Auto> completeAuto(String query) {
		List<Auto> suggestions = new ArrayList<Auto>();
		for (Auto auto : Auto.findAllAutos()) {
			String autoStr = String.valueOf(auto.getMatricula());
			if (autoStr.toLowerCase().startsWith(query.toLowerCase())) {
				suggestions.add(auto);
			}
		}
		return suggestions;
	}
}
