package com.sebastian.web;

import java.util.List;

import javax.faces.bean.ManagedBean;

import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;
import org.springframework.roo.addon.serializable.RooSerializable;

import com.sebastian.model.Comercio;

@ManagedBean(name = "comercioBean")
@RooSerializable
@RooJsfManagedBean(entity = Comercio.class, beanName = "comercioBean")
public class ComercioBean {

	private List<Comercio> allComercios;

	public List<Comercio> getAllComercios() {
		return allComercios = Comercio.findAllComercios();
	}

	public void setAllComercios(List<Comercio> allComercios) {
		this.allComercios = allComercios;
	}

}
