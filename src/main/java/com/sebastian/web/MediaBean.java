package com.sebastian.web;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.logging.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;
import org.springframework.roo.addon.jsf.managedbean.RooJsfManagedBean;

import com.sebastian.service.MediaServiceImpl;

@SessionScoped
@ManagedBean(name = "mediaBean")
@RooJsfManagedBean(beanName = "mediaBean", entity = MediaServiceImpl.class)
public class MediaBean {

	private final static Logger log = Logger.getLogger(MediaBean.class
			.getName());

	private MediaServiceImpl media;
	private StreamedContent fileToDownload;
	private UploadedFile fileToUpload;
	private String file_id;
	private String file_type;
	private String[] imageTypes = { "jpg", "jpeg", "gif", "bmp" };
	private String[] applicationTypes = { "pdf", "doc", "xml" };
	private String PATH = System.getProperty("java.io.tmpdir");

	public MediaBean() {
		media = MediaServiceImpl.getInstance();
	}

	public String upload(UploadedFile uF) {
		try {
			String fileId = media.push(uF.getInputstream(), uF.getFileName());
			return fileId;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}

	/**
	 * Determines the name of the folder in which be stored depending of the
	 * file extension
	 * 
	 * @param extension
	 *            the file extension (e.x. '.jpg')
	 * @return the name of the folder
	 */
	private String defineFileType(String extension) {
		for (String imageExtension : imageTypes) {
			if (extension.equals(imageExtension)) {
				return "image";
			}
		}
		for (String appExtension : applicationTypes) {
			if (extension.equals(appExtension)) {
				return "application";
			}
		}
		return "taller";
	}

	/**
	 * Retrieve a file from the files database
	 * 
	 * @return a StreamedContent to be readed; null if 'file_id' was not defined
	 *         in the JSF
	 */
	public StreamedContent getFileToDownload() {
		// The 'file_id', 'file_type' and 'cache' params are defined in a JSF
		// file (e.x. p:graphicImage)
		file_id = FacesContext.getCurrentInstance().getExternalContext()
				.getRequestParameterMap().get("file_id");

		if (file_id != null) {
			FileInputStream saveFile;
			try {
				saveFile = new FileInputStream(PATH + "/" + file_id);
				log.info("Getting the file locally from " + PATH);
				return new DefaultStreamedContent(saveFile);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			// Cannot return null
			fileToDownload = new DefaultStreamedContent();
		}
		return fileToDownload;
	}

	public void setFileToDownload(StreamedContent fileToDownload) {
		this.fileToDownload = fileToDownload;
	}

	public MediaServiceImpl getMedia() {
		return media;
	}

	public void setMedia(MediaServiceImpl media) {
		this.media = media;
	}

	public UploadedFile getFileToUpload() {
		return fileToUpload;
	}

	public void setFileToUpload(UploadedFile fileToUpload) {
		this.fileToUpload = fileToUpload;
	}

	public String getFile_type() {
		return file_type;
	}

	public void setFile_type(String file_type) {
		this.file_type = file_type;
	}

	public String getFile_id() {
		return file_id;
	}

	public void setFile_id(String file_id) {
		this.file_id = file_id;
	}

}
