package com.sebastian.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Presupuesto {

	@NotNull
	@ManyToOne
	private Auto auto;

	private double precioTotal = 0;

	private List<String> imagesPath = new ArrayList<String>();

	private double totalManoObra = 0;
	
	private double totalCompra = 0;

	@Size(max = 2500)
	private String observaciones;

	@NotNull
	@ManyToMany(cascade = CascadeType.ALL)
	private List<LineaCompra> listaCompras = new ArrayList<LineaCompra>();

	@NotNull
	@ManyToMany(cascade = CascadeType.ALL)
	private List<LineaManoObra> listaManoObra = new ArrayList<LineaManoObra>();

	public List<LineaManoObra> getListaManoObra() {
		return listaManoObra;
	}

	public void setListaManoObra(List<LineaManoObra> listaManoObra) {
		this.listaManoObra = listaManoObra;
	}
	
	public List<LineaCompra> getListaCompras() {
		return listaCompras;
	}

	public void setListaCompras(List<LineaCompra> listaCompras) {
		this.listaCompras = listaCompras;
	}

	public double getTotalCompra() {
		return totalCompra;
	}

	public void setTotalCompra(double totalCompra) {
		this.totalCompra = totalCompra;
	}

	public List<String> getImagesPath() {
		return imagesPath;
	}

	public void setImagesPath(List<String> imagesPath) {
		this.imagesPath = imagesPath;
	}
	
}
