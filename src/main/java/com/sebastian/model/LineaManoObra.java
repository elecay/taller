package com.sebastian.model;

import javax.persistence.CascadeType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class LineaManoObra {
	

	private Integer horas;

	private Double precioTotal;

	@OneToOne(cascade = CascadeType.ALL)
	private ManoObra manoDeObra;
	
	public LineaManoObra(){
		manoDeObra = new ManoObra();
	}
}
