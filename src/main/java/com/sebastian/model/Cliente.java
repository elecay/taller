package com.sebastian.model;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class Cliente {

    private String nombre;

    private String apellido;

    private String cedula;

    private String direccion;

    private String ciudad;

    private String telefono;

    private String celular;

    private String email;
    
}
