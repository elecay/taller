// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.sebastian.model;

import com.sebastian.model.Presupuesto;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;

privileged aspect Presupuesto_Roo_Jpa_Entity {
    
    declare @type: Presupuesto: @Entity;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private Long Presupuesto.id;
    
    @Version
    @Column(name = "version")
    private Integer Presupuesto.version;
    
    public Long Presupuesto.getId() {
        return this.id;
    }
    
    public void Presupuesto.setId(Long id) {
        this.id = id;
    }
    
    public Integer Presupuesto.getVersion() {
        return this.version;
    }
    
    public void Presupuesto.setVersion(Integer version) {
        this.version = version;
    }
    
}
