package com.sebastian.model;

import javax.persistence.ManyToOne;
import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord(entityName = "Auto")
public class Auto {

    private String modelo;

    private String motor;

    private String transmision;

    private Integer ano;

    private String combustion;

    private String matricula;

    private String color;

    private String marca;

    private String poliza;

    @ManyToOne
    private Cliente cliente;

    @ManyToOne
    private Aseguradora aseguradora;
    
}
