package com.sebastian.model;

import org.springframework.roo.addon.javabean.RooJavaBean;
import org.springframework.roo.addon.jpa.activerecord.RooJpaActiveRecord;
import org.springframework.roo.addon.tostring.RooToString;

@RooJavaBean
@RooToString
@RooJpaActiveRecord
public class LineaCompra {

    private Integer cantidad;

    private Double precioTotal;

    private String descripcion;
    
    private String codigo;

    private Double precio;
    
    private Comercio comercio;
}
